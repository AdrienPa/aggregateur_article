from flask import Flask, request, render_template, url_for, escape, redirect
import feedparser
import psycopg2
from psycopg2 import Error
import locale
import insertion_bdd
import connexion

locale.setlocale(locale.LC_TIME,'')

app = Flask(__name__)

@app.route('/', methods=['GET'])
def get_home():
    offre = nombre_article(connexion.connection_postgress())  
    datas = articles(connexion.connection_postgress())
    data = [offre,datas]
    types = type_source(connexion.connection_postgress())
    return render_template('pages/home.html', data = datas, offre = offre, types = types)

@app.route('/menu_sources', methods=['POST'])
def get_nav():     
    insertion_bdd.Suppr_source(connexion.connection_postgress(), request.form.get('check'))
    return redirect(url_for('get_home'))

@app.route('/ajout_source', methods=['POST'])
def get_ajout():
    url = request.form.get('url')
    d = feedparser.parse(url)
    v = d['version']

    if url == '':
        return redirect(url_for('get_home'))
    elif 'rss' in v or 'atom' in v:
        insertion_bdd.insertionTables(connexion.connection_postgress(), request.form.get('url'))
        insertion_bdd.suppression_doublon(connexion.connection_postgress())
        return redirect(url_for('get_home'))
    else:
        return redirect(url_for('get_home'))    

def deconnexion_DB(connection):
    conn = connection
    if(conn):
        conn.close()

def articles(connection):
    cursor = connection.cursor()
    articles = ''' select source, title, pubdate, description, link from agregat_rss order by pubdate desc limit 30; '''

    cursor.execute(articles)
    connection.commit()
    files = cursor.fetchall()
    return files

    deconnexion_DB(connexion.connection_postgress())

def nombre_article(connection):
    conn = connection
    cursor = conn.cursor()
    select = ''' select count (title) from agregat_rss; '''

    cursor.execute(select)
    conn.commit()

    files = cursor.fetchall()
    for elem in files:

        return elem[0]
    deconnexion_DB(connexion.connection_postgress())

def type_source(connection):
    conn = connection
    cursor = conn.cursor()
    select = ''' SELECT DISTINCT source FROM agregat_rss; '''

    cursor.execute(select)
    conn.commit()

    files = cursor.fetchall()
    
    return files

if __name__=="__main__":
    deconnexion_DB(connexion.connection_postgress())

