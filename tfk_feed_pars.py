import feedparser
'''
fichier qui récupere un flux de donnée au format rss en xml ou atom et retourne 
    un dictionnaire par article du flux dans une liste
'''
def t_feed_parse(url):
    '''
    récupère une liste de dictionnaire     
    '''    
    d = feedparser.parse(url)
    e = d['entries']
    v = d['version']
  
    if 'rss' in v :
      idx = {'title':'title','pubdate':'published','link':'link','description':'description'}  
    elif 'atom' in v :
      idx = {'title':'title','pubdate':'updated','link':'link','description':'summary'}
  
    la_liste = [] # création d'une liste vide

    for elt in e:
      annonce = {} # on initialise un dictionnaire vide
      annonce.update([('source',url),('title', elt['title']),('pubdate', elt[idx['pubdate']]),('link', elt['link']), ('description', elt[idx['description']]) ])
      la_liste.append(annonce) # on ajoute le dictionnaire à la liste
    return la_liste
