import connexion
import psycopg2
from psycopg2 import Error

def CreateTable(connection):
    cursor = connection.cursor()
    
    sql = """CREATE TABLE if not exists RSS (
        title text,
        pubdate text,
        link text,
        description text
    );"""

    cursor.execute(sql)
    connection.commit()

if __name__=="__main__":
    CreateTable(connexion.connection_postgress())