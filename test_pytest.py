import pytest
from tfk_feed_pars import t_feed_parse

src = 'https://news.ycombinator.com/rss'
a_test = t_feed_parse(src)

def test_feed():
    assert type(a_test) == list
    assert type(a_test) != None
    assert a_test != list()